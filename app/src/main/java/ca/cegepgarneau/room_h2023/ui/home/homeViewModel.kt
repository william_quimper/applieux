package ca.cegepgarneau.room_h2023

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ca.cegepgarneau.room_h2023.db.LieuxDao
import ca.cegepgarneau.room_h2023.db.dtb
import ca.cegepgarneau.room_h2023.db.model.Lieux

class homeViewModel(application: Application) : AndroidViewModel(application) {

    private val lieuDao: LieuxDao = dtb.getInstance(application).LieuxDao()

    private val _text = MutableLiveData<Boolean>().apply {
        value = true
    }
    val text: LiveData<Boolean> = _text

    fun insertLieu(lieux: Lieux) {
        lieuDao.insertLieux(lieux)
    }
    fun getAllLieux(): LiveData<List<Lieux>> {
        return lieuDao.getAllLieux()
    }

    fun deleteLieu(lieu: Lieux){
        return lieuDao.deleteLieux(lieu)
    }

    fun modifyLieu(lieu: Lieux){
        return lieuDao.updateLieux(lieu)
    }

}