package ca.cegepgarneau.room_h2023.ui.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.room_h2023.R
import ca.cegepgarneau.room_h2023.db.LieuxDao
import ca.cegepgarneau.room_h2023.databinding.FraguementAdminBinding
import ca.cegepgarneau.room_h2023.ui.home.FirstFragmentArgs
import kotlin.properties.Delegates

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AdminFragment : Fragment() {

    private var _binding: FraguementAdminBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var _lieuxDao: LieuxDao
    private var _isAdmin : Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _isAdmin = AdminFragmentArgs.fromBundle(requireArguments()).isAdmin

        _binding = FraguementAdminBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar = activity?.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        toolbar?.visibility = View.GONE

        binding.switch1.isChecked = _isAdmin
        binding.btnConfirmerAdmin.setOnClickListener {
            val action: NavDirections =
                AdminFragmentDirections.actionAdminFragmntToFirstFragment(binding.switch1.isChecked)
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}