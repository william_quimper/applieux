package ca.cegepgarneau.room_h2023.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.room_h2023.R
import ca.cegepgarneau.room_h2023.databinding.FragmentFirstBinding
import ca.cegepgarneau.room_h2023.homeViewModel
import ca.cegepgarneau.room_h2023.db.model.Lieux
import ca.cegepgarneau.room_h2023.db.model.LieuxAdapter
import java.lang.reflect.InvocationTargetException
import kotlin.concurrent.thread

import com.google.android.material.bottomnavigation.BottomNavigationView


class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    var isAdmin: Boolean = false

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var mLieuxList: MutableList<Lieux> = ArrayList()
    private lateinit var rvLieux: RecyclerView
    lateinit var adapter: LieuxAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)

        try {
            val homeViewModel =
                ViewModelProvider(requireActivity())[homeViewModel::class.java]
            val newLieux = homeViewModel.getAllLieux()

            newLieux.observe(viewLifecycleOwner) { lieuxList ->
                mLieuxList.clear()
                for (lieu in lieuxList) {
                    mLieuxList.add(lieu)
                    println(lieu.id)
                }
                adapter.notifyDataSetChanged()
            }

            // Gestion du RecyclerView
            rvLieux = binding.rvPerson
            // fixe les dimensions du RecyclerView pour gain de performance
            rvLieux.setHasFixedSize(true)
            // Création de l'adapter
            isAdmin = FirstFragmentArgs.fromBundle(requireArguments()).isAdmin
            adapter = LieuxAdapter(mLieuxList,isAdmin)
            // Lier l'adapter au RecyclerView
            rvLieux.adapter = adapter

            val onItemClickListener: LieuxAdapter.OnItemClickListenerInterface =
                object : LieuxAdapter.OnItemClickListenerInterface {
                    override fun onItemClick(itemView: View?, position: Int) {
                        Toast.makeText(
                            context, "Lieu Ajouté", Toast.LENGTH_SHORT
                        ).show()
                    }

                    // Méthode appelée lors du clic sur le bouton Éditer
                    override fun onClickEdit(itemView: View, position: Int) {
                        Toast.makeText(
                            context, "Clic edit sur ${mLieuxList[position]}", Toast.LENGTH_SHORT
                        ).show()

                        val dialogView = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_edit, null)
                        val editText1 = dialogView.findViewById<EditText>(R.id.edit_text_title)
                        val editText2 = dialogView.findViewById<EditText>(R.id.edit_text_Adress)
                        val editText3 = dialogView.findViewById<EditText>(R.id.edit_text_Desc)

                        AlertDialog.Builder(requireContext())
                            .setTitle("Enter new value")
                            .setView(dialogView)
                            .setPositiveButton("OK") { _, _ ->
                                val title = editText1.text.toString()
                                val adress = editText2.text.toString()
                                val desc = editText3.text.toString()

                                thread {
                                    homeViewModel.modifyLieu(Lieux(mLieuxList[position].id,title,desc,adress,mLieuxList[position].image ))
                                }
                            }
                            .setNegativeButton("Cancel") { _, _ -> }
                            .show()

                    }

                    override fun onClickDelete(position: Int) {
                        if(mLieuxList.isEmpty()){
                            adapter.notifyDataSetChanged()
                        }
                        else{
                            thread {
                                homeViewModel.deleteLieu(mLieuxList[position])
                            }
                        }
                    }
                }
            adapter.setOnItemClickListener(onItemClickListener)
        } catch (e: InvocationTargetException) {
            val cause: Throwable = e
        }

        //Réglage d'affichage du recyclerView
        rvLieux.layoutManager = GridLayoutManager(requireContext(), 1)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toolbar = activity?.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        toolbar?.visibility = View.VISIBLE

        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
        binding.newItem2.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_AddLieuxFragement)
        }
        R.id.Visited
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}