package ca.cegepgarneau.room_h2023.db

import androidx.lifecycle.LiveData
import androidx.room.*
import ca.cegepgarneau.room_h2023.db.model.Lieux

@Dao
interface LieuxDao {

    /**
     * dao.insertLieux(lieux)
     */
    @Insert
    fun insertLieux(lieux: Lieux)

    /**
     * val id = dao.insertLieuxReturnId(lieux)
     */
    @Insert
    fun insertLieuxReturnId(lieux: Lieux): Long

    /**
     * lieux = arrayOf(lieux1, lieux2, lieux3)
     * val ids = dao.insertLieux(lieux)
     */
    @Insert
    fun insertLieux(lieux: List<Lieux>): List<Long>

    /**
     * dao.updateLieux(lieux)
     */
    @Update
    fun updateLieux(lieux: Lieux)

    @Delete
    fun deleteLieux(lieux: Lieux)

    /**
     * val lieux = dao.getLieuxById(1).observe(this, { lieux -> ... })
     */
    @Query("SELECT * FROM lieux WHERE id = :id")
    fun getLieuxById(id: Int): LiveData<Lieux>

    /**
     * val lieux = dao.getLieux().observe(this, { lieux -> ... })
     */
    @Query("SELECT * FROM lieux")
    fun getAllLieux(): LiveData<List<Lieux>>

    /**
     * delete all lieux
     */
    @Query("DELETE FROM lieux")
    fun deleteAllLieux()

}
