package ca.cegepgarneau.room_h2023.db.model

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.room_h2023.R
import java.io.File

class LieuxAdapter(private val lieux: List<Lieux>, private val isAdmin: Boolean) :
    RecyclerView.Adapter<LieuxAdapter.ViewHolder>(){

    // Interface pour gérer les clics sur les éléments de la liste
    interface OnItemClickListenerInterface {
        fun onItemClick(itemView: View?, position: Int)
        fun onClickEdit(itemView: View, position: Int)
        fun onClickDelete(position: Int)

    }

    // Objet qui implémente l'interface OnItemClickListener
    lateinit var listener: OnItemClickListenerInterface

    // Cette méthode permet de définir l'objet qui implémente l'interface OnItemClickListener
    // Elle est appelée dans la classe MainActivity
    fun setOnItemClickListener(listener: OnItemClickListenerInterface) {
        this.listener = listener
    }

    /**
     * Classe interne représentant les pointeurs vers les composants graphiques d'une ligne de la liste
     * Il y aura une instance de cette classe par ligne de la liste
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tv_name)
        var tvDescription: TextView = itemView.findViewById(R.id.tv_description)
        var imgPerson: ImageView = itemView.findViewById(R.id.img_person)
        var tvAdresse: TextView = itemView.findViewById(R.id.tv_adresse2)

        // Constructeur
        init {
            // Ajoute un écouteur d'événement du click sur itemView (la ligne entière)
            itemView.setOnClickListener {
                // Récupère la position de l'élément cliqué
                val position = adapterPosition
                // Vérifie que la position est valide
                // (parfois, le clic est détecté alors que la position n'est pas encore déterminée)
                if (position != RecyclerView.NO_POSITION) {
                    // Appelle la méthode onItemClick de l'objet qui implémente l'interface OnItemClickListener
                    listener.onItemClick(itemView, position)

                }
            }

            // Ajoute un écouteur d'événement du click long sur itemView
            // (pour afficher le menu contextuel)
            itemView.setOnCreateContextMenuListener { menu, v, menuInfo ->
                val position = adapterPosition
                // Crée les items du menu contextuel
                val edit: android.view.MenuItem = menu.add(0, v.id, 0, "Editer")
                val delete: android.view.MenuItem = menu.add(0, v.id, 0, "Supprimer")

                if (!isAdmin){
                    edit.isVisible = false
                    delete.isVisible = false
                }
                // Ajoute un écouteur d'événement sur les items du menu contextuel
                edit.setOnMenuItemClickListener {
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onClickEdit(itemView, position)
                    }
                    false
                }
                delete.setOnMenuItemClickListener {
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onClickDelete(position)
                    }
                    false
                }
            }
        }
    }


    // Cette méthode est appelée à chaque fois qu'il faut créer une ligne
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Utilise le layout person_one_line pour créer une vue
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.lieux_one_line, parent, false)

        // Crée un viewHolder en passant la vue en paramètre
        return ViewHolder(view)
    }

    // Cette méthode permet de lier les données à la vue
    // Elle remplit une ligne créée par onCreateViewHolder
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Récupère l'élément de la liste à la position "position"
        val lieu: Lieux = lieux[position]
        // Met à jour les données de la vue
        holder.tvName.text = lieu.name
        holder.tvDescription.text = lieu.description
        holder.tvAdresse.text = lieu.lieux

        var path_to_saved_file : String = lieu.image

        val savedImagePath = path_to_saved_file // replace with the file path of your saved image
        val savedImageFile = File(savedImagePath)
        val savedImageBitmap = BitmapFactory.decodeFile(savedImageFile.absolutePath)

        holder.imgPerson.setImageBitmap(savedImageBitmap)
    }

    // Cette méthode permet de retourner le nombre d'éléments de la liste
    override fun getItemCount(): Int {
        return lieux.size
    }

}


