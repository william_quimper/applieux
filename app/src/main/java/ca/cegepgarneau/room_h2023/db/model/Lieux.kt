package ca.cegepgarneau.room_h2023.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "lieux")
data class Lieux(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "info")
    var name: String,
    var description: String,
    var lieux: String,
    var image : String

//
//    @Ignore
//    var imageBitmap: Bitmap?
)






