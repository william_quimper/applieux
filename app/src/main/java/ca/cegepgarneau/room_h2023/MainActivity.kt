package ca.cegepgarneau.room_h2023

import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import ca.cegepgarneau.room_h2023.databinding.ActivityMainBinding
import ca.cegepgarneau.room_h2023.db.LieuxDao
import ca.cegepgarneau.room_h2023.db.dtb
import ca.cegepgarneau.room_h2023.db.model.Lieux
import ca.cegepgarneau.room_h2023.ui.home.FirstFragmentDirections

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var lieuDao: LieuxDao
    private lateinit var binding: ActivityMainBinding
    var isAdmin: Boolean = false
    private lateinit var _viewedItems : ArrayList<Lieux>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Récupération de la base de données et du DAO
        lieuDao = dtb.getInstance(this).LieuxDao()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.AdminFragment, R.id.SecondFragment
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val navController = findNavController(R.id.nav_host_fragment_content_main)
                val navBackStackEntry = navController.currentBackStackEntry
                navBackStackEntry?.savedStateHandle?.get<Boolean>("isAdmin")?.let { isAdmin ->
                    this.isAdmin = isAdmin
                }
                val action: NavDirections =
                    FirstFragmentDirections.actionFirstFragmentToAdminFragment(isAdmin)
                findNavController(R.id.nav_host_fragment_content_main).navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


        override fun onCreateOptionsMenu(menu: Menu): Boolean {
            // Inflate the menu; this adds items to the action bar if it is present.
            menuInflater.inflate(R.menu.menu_main, menu)
            return true
        }

        override fun onSupportNavigateUp(): Boolean {
            val navController = findNavController(R.id.nav_host_fragment_content_main)
            return navController.navigateUp(appBarConfiguration)
                    || super.onSupportNavigateUp()
        }
}