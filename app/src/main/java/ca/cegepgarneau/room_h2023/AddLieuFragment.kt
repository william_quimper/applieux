package ca.cegepgarneau.room_h2023

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.room_h2023.databinding.FragmentAjoutlieuxBinding
import ca.cegepgarneau.room_h2023.db.model.Lieux
import ca.cegepgarneau.room_h2023.ui.admin.AdminFragmentDirections
import ca.cegepgarneau.room_h2023.ui.home.FirstFragmentArgs
import java.io.*
import java.util.*
import kotlin.concurrent.thread

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddLieuFragment : Fragment() {

    private var _binding: FragmentAjoutlieuxBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var imageIV: ImageView
    private var savedImage: Uri? = null
    var _isAdmin : Boolean = false
    private lateinit var pickMedia: ActivityResultLauncher<PickVisualMediaRequest>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAjoutlieuxBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _isAdmin = FirstFragmentArgs.fromBundle(requireArguments()).isAdmin

        val exampleViewModel = ViewModelProvider(requireActivity())[homeViewModel::class.java]

        binding.imgChooser.setOnClickListener{
            imageIV = binding.idIVImage
            choosePhotoFromGallery()
        }

        // On enregistre le callback pour l'activité de sélection d'image
        pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            // Le callback est appelé avec le résultat de l'activité
            if (uri != null) {
                imageIV.setImageURI(uri)
                savedImage = saveImageToInternalStorage(uri)
                Log.d("PhotoPicker", "Selected URI: $uri")
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

        binding.btnConfirmer.setOnClickListener {
            val inputDescription = binding.editTextDescription.text.toString()
            val inputLieu = binding.editTextLieux.text.toString()
            val inputAdresse = binding.editTextAdresse.text.toString()

            if (inputLieu.isNotEmpty() && inputDescription.isNotEmpty() && inputAdresse.isNotEmpty() && savedImage?.path != null && savedImage?.path!!.isNotEmpty()) {
                thread {
                    // Ajout d'un utilisateur quand on clique sur le bouton
                    val newLieux = Lieux(0, inputLieu, inputDescription, inputAdresse, savedImage!!.path.toString())
                    exampleViewModel.insertLieu(newLieux)

                }
                val action: NavDirections =
                    AddLieuFragmentDirections.actionAddLieuToFirstFragment(_isAdmin)
                findNavController().navigate(action)
            }
            else {
                // Show an error message and prevent the dialog from closing
                Toast.makeText(
                    requireContext(),
                    "Veuillez remplir tous les champs",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    /**
     * Cette méthode permet de sauvegarder une image dans le dossier interne de l'application
     * @param uri L'URI de l'image à sauvegarder
     * @return L'URI de l'image sauvegardée
     */
    private fun saveImageToInternalStorage(uri: Uri): Uri {
        val wrapper = ContextWrapper(context)
        var file = wrapper.getDir("TP2", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.jpg")
        // convert uri to bitmap
        val bitmap =
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(context?.contentResolver!!, uri))
        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return Uri.parse(file.absolutePath)
    }

    /**
     * Cette méthode permet de choisir une image dans la galerie
     */
    private fun choosePhotoFromGallery() {
        pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}


